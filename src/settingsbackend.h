#ifndef SETTINGSBACKEND_H
#define SETTINGSBACKEND_H
#include <QObject>
#include <QNetworkReply>
#include <Sailfish/Secrets/secretmanager.h>


class SettingsBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool notificationDisabled READ notificationDisabled WRITE setNotificationDisabled NOTIFY notificationDisabledChanged)
    Q_PROPERTY(int notifInterval READ notifInterval WRITE setNotifInterval NOTIFY notifIntervalChanged)
    Q_PROPERTY(int sortType READ sortType WRITE setSortType NOTIFY sortTypeChanged)
    Q_PROPERTY(bool backgroundServiceDisabled READ backgroundServiceDisabled WRITE setBackgroundServiceDisabled NOTIFY backgroundServiceDisabledChanged)

public:
    SettingsBackend();
    Q_INVOKABLE void clear_cache();
    Q_INVOKABLE void clear_config();
    bool notificationDisabled();
    void setNotificationDisabled(bool n);
    bool backgroundServiceDisabled();
    void setBackgroundServiceDisabled(bool n);
    int notifInterval();
    void setNotifInterval(int n);
    int sortType();
    void setSortType(int n);
signals:
    void done();
    void configClearDone();
    void notificationDisabledChanged();
    void notifIntervalChanged();
    void sortTypeChanged();
    void backgroundServiceDisabledChanged();
private:
    Sailfish::Secrets::SecretManager m_secretManager;
    bool m_notification_disabled;
    int m_notif_interval = 0;
    int m_sort_type = 0;
    bool m_background_service_disabled;
private slots:
    void after_logout(QNetworkReply* reply);

};

#endif // SETTINGSBACKEND_H
