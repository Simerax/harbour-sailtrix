#include "createroombackend.h"
#include <QJsonDocument>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonArray>
#include "enc-util.h"

CreateRoomBackend::CreateRoomBackend(QObject *parent) : QObject(parent), manager { new QNetworkAccessManager(this) }
{
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        m_hs = document.object().value("homeserver").toString();
        connect(manager, &QNetworkAccessManager::finished, this, &CreateRoomBackend::done);
    }
}

CreateRoomBackend::~CreateRoomBackend() {
    delete manager;
}

QString CreateRoomBackend::hs() {
    return m_hs;
}

void CreateRoomBackend::create_public(QString alias, QString name, QString topic) {
    qDebug() << "Creating Public Room: " << name << topic << alias;

    QJsonObject to_send;
    to_send.insert("visibility", "public");
    to_send.insert("room_alias_name", alias);
    to_send.insert("name", name);
    to_send.insert("topic", topic);
    to_send.insert("preset", "public_chat");

    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/createRoom");
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    manager->post(req, QJsonDocument(to_send).toJson());
}

void CreateRoomBackend::create_private(QString name, QString topic, bool encrypt){
    qDebug() << "Creating Private Room: " << name << topic << encrypt;
    QJsonObject to_send;
    to_send.insert("visibility", "private");
    to_send.insert("name", name);
    to_send.insert("topic", topic);
    to_send.insert("preset", "private_chat");

    if (encrypt) {
        QJsonObject m_room_encryption;
        m_room_encryption.insert("type", "m.room.encryption");
        QJsonObject content;
        content.insert("algorithm", "m.megolm.v1.aes-sha2");
        m_room_encryption.insert("content", content);

        QJsonArray init_state;
        init_state.append(m_room_encryption);
        to_send.insert("initial_state", init_state);
    }

    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/createRoom");
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    manager->post(req, QJsonDocument(to_send).toJson());
}

void CreateRoomBackend::done(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());

        emit created(doc.object().value("room_id").toString());
    }
}
